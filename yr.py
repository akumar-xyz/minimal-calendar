#!/usr/bin/env python

import cairo
import datetime
import math
import decimal
from optparse import OptionParser

# Parsing options (year and output file)
parser = OptionParser()
parser.add_option("-y", "--year", dest="year",
                  default=datetime.datetime.now().year,
                  type="int",
                  help="Year to print calendar (default: current year from local time)")

parser.add_option("-o", "--output-file", dest="output_file", default="cal.pdf",
        help="Output FILE (default: cal.pdf)")
(options, args) = parser.parse_args()
year = options.year
output_file = options.output_file

# Set the font here
font = 'Monospace'

# Height and width (in pixels I think)
# Just trial and error, '6' seems to work best
height = 210*6
width = 297*6

# Grid size
# 15*25=375 days will be generated.
x_size = 15
y_size = 25


sq_size = (width / (y_size + (width/500)))

# Center the calendar to the page
x_padding = (height - (sq_size * (x_size)))/2
y_padding = (width - (sq_size * (y_size)))/2


# Logic borrowed from https://gist.github.com/miklb/ed145757971096565723
# not completely sure how it works. Algorithm adapted to check if it is a full
# moon day or not
def is_full_moon(now):
    dec = decimal.Decimal
    diff = now - datetime.datetime(2001, 1, 1)
    days = dec(diff.days) + (dec(diff.seconds) / dec(86400))
    lunations = dec("0.20439731") + (days * dec("0.03386319269"))
    pos = lunations % dec(1)
    index = (pos * dec(8)) + dec("0.5")
    if (math.fabs(index - dec("4.5")) < 0.2):
        return True
    else:
        return False


# Python generator to generate one day at a time
def inc():
    date_seed = datetime.datetime(year, 1, 1)
    date_seed -= datetime.timedelta(days=1)
    while True:
        date_seed += datetime.timedelta(days=1)
        yield date_seed


days = inc()

# Drawing
with cairo.PDFSurface(output_file, height, width) as surface:
    context = cairo.Context(surface)

    context.select_font_face(
        font, cairo.FONT_SLANT_NORMAL, cairo.FONT_WEIGHT_NORMAL)

    context.move_to(x_padding, y_padding - 10)
    context.show_text("cal: " + str(year))
    for y in range(0, y_size):
        for x in range(0, x_size):
            p_x = x * sq_size + x_padding
            p_y = y * sq_size + y_padding
            context.rectangle(p_x, p_y, sq_size, sq_size)

            d_date = next(days)
            day = d_date.day
            label = str(day)
            extra = ""
            if day == 1:
                extra += d_date.strftime('%b ')

            if is_full_moon(d_date):
                extra += "○"
            if extra:
                context.move_to(p_x + 0.1 * sq_size, p_y + 0.25 * sq_size)
                context.show_text(extra)

            context.move_to(p_x + 0.75 * sq_size, p_y + 0.25 * sq_size)
            context.show_text(label)
            if d_date.weekday() == 6:
                context.select_font_face(
                    font, cairo.FONT_SLANT_NORMAL, cairo.FONT_WEIGHT_BOLD)
                context.move_to(p_x + 0.75 * sq_size, p_y + 0.25 * sq_size)
                context.show_text("_"*len(label))
                context.select_font_face(
                    font, cairo.FONT_SLANT_NORMAL, cairo.FONT_SLANT_NORMAL)

    context.stroke()
