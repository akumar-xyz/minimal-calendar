# minimal-calendar

Simple minimalist calendar generator. Useful for self tracking purposes etc.

Uses python and cairo to generate PDF files.

## Documentation

Numbers which are underlined are Sundays

Dates that have full moon have a circle in them 

### Usage
```
Usage: yr.py [options]

Options:
  -h, --help            show this help message and exit
  -y YEAR, --year=YEAR  Year to print calendar (default: current year from local time)
  -o OUTPUT_FILE, --output-file=OUTPUT_FILE
                        Output FILE (default: cal.py)
```

